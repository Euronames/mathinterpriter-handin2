/*
 * generated by Xtext 2.21.0
 */
package handin2;


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
public class MathInterpriterStandaloneSetup extends MathInterpriterStandaloneSetupGenerated {

	public static void doSetup() {
		new MathInterpriterStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}
