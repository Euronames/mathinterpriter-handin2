/**
 * generated by Xtext 2.21.0
 */
package handin2.mathInterpriter;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exp Op</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see handin2.mathInterpriter.MathInterpriterPackage#getExpOp()
 * @model
 * @generated
 */
public interface ExpOp extends EObject
{
} // ExpOp
